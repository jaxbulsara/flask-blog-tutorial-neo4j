from py2neo.database import Graph, Schema
import json

graph = Graph(password="root")
schema = Schema(graph)

schema_definition = json.load(open("db_schema.json", "r"))

existing_indexes = {
    label: schema.get_indexes(label) for label in schema.node_labels
}
existing_uniqueness_constraints = {
    label: schema.get_uniqueness_constraints(label)
    for label in schema.node_labels
}

for uniqueness_constraint in schema_definition["uniqueness_constraints"]:
    label = uniqueness_constraint["label"]
    property_key = uniqueness_constraint["property_key"]

    if property_key not in schema.get_uniqueness_constraints(label):
        print(f"Creating uniqueness constraint: ('{label}', {property_key})")
        schema.create_uniqueness_constraint(label, property_key)

    else:
        existing_uniqueness_constraints[label].remove(property_key)
        existing_indexes[label].remove((property_key,))


for index in schema_definition["indexes"]:
    label = index["label"]
    property_keys = tuple(index["property_keys"])

    if property_keys not in schema.get_indexes(label):
        print(f"Creating index: ('{label}', {property_keys})")
        schema.create_index(label, *property_keys)

    else:
        existing_indexes[label].remove(property_keys)


for label, uniqueness_constraints in existing_uniqueness_constraints.items():
    for constraint in uniqueness_constraints:
        print(f"Removing uniqueness constraint: ('{label}', {constraint})")
        schema.drop_uniqueness_constraint(label, constraint)
        existing_indexes[label].remove((constraint,))


for label, indexes in existing_indexes.items():
    for index in indexes:
        print(f"Removing index: ('{label}', {index})")
        schema.drop_index(label, *index)
