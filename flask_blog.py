from forms import RegistrationForm, LoginForm

import hashlib
import bcrypt

from flask import Flask, render_template, url_for, flash, redirect
from datetime import datetime
from py2neo.ogm import GraphObject, Property, RelatedFrom, RelatedTo
from py2neo.data import Relationship

app = Flask(__name__)
app.config["SECRET_KEY"] = "383e129399991863c7cd7dc8a0c41f6b"


class Post(GraphObject):
    title = Property()
    date_posted = Property()
    content = Property()
    title_date_posted = Property()

    author = RelatedFrom("User", "AUTHOR")

    def __init__(self, title, date_posted, content):
        self.title = title
        self.date_posted = str(date_posted)
        self.content = content

    def __repr__(self):
        return f"{self.__node__}"


class User(GraphObject):
    username = Property()
    email = Property()
    image_file = Property()
    password = Property()

    posts = RelatedTo("Post", "AUTHOR")

    def __init__(self, username, email, password, image_file=None):
        self.username = username
        self.email = email
        self.encrypt_password(password)

        if image_file:
            self.image_file = image_file

    def __repr__(self):
        return f"{self.__node__}"

    def encrypt_password(self, password):
        salt = bcrypt.gensalt()
        self.password = bcrypt.hashpw(password.encode("utf-8"), salt)

    def check_password(self, input_password):
        return bcrypt.checkpw(input_password, self.password)


class AUTHOR(Relationship):
    pass


post_data = [
    {
        "author": "Jay Bulsara",
        "title": "Blog Post 1",
        "content": "First post!",
        "timestamp": "2020-01-05",
    },
    {
        "author": "Jay Bulsara",
        "title": "Blog Post 2",
        "content": "Second post!",
        "timestamp": "2020-01-07",
    },
]


@app.route("/")
@app.route("/home")
def home():
    return render_template("home.html", posts=post_data)


@app.route("/about/")
def about():
    return render_template("about.html", title="About")


@app.route("/register", methods=["GET", "POST"])
def register():
    form = RegistrationForm()

    if form.validate_on_submit():
        message = f"Account created for {form.username.data}!"
        category = "success"
        flash(message, category)
        return redirect(url_for("home"))

    return render_template("register.html", title="Register", form=form)


@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if (
            form.email.data == "admin@blog.com"
            and form.password.data == "password"
        ):
            message = "You have been logged in."
            category = "success"
            flash(message, category)
            return redirect(url_for("home"))
        else:
            message = "Login unsuccessful. Please check username and password."
            category = "danger"
            flash(message, category)

    return render_template("login.html", title="Login", form=form)


if __name__ == "__main__":
    app.run(debug=True)
