from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo


class RegistrationForm(FlaskForm):
    username_validators = [DataRequired(), Length(min=2, max=20)]
    username = StringField("Username", validators=username_validators)

    email_validators = [DataRequired(), Email()]
    email = StringField("Email", validators=email_validators)

    password_validators = [DataRequired()]
    password = PasswordField("Password", validators=password_validators)

    confirm_password_validators = [
        DataRequired(),
        EqualTo("password"),
    ]
    confirm_password = PasswordField(
        "Confirm Password", validators=confirm_password_validators
    )

    submit = SubmitField("Sign Up")


class LoginForm(FlaskForm):
    email_validators = [DataRequired(), Email()]
    email = StringField("Email", validators=email_validators)

    password_validators = [DataRequired()]
    password = PasswordField("Password", validators=password_validators)

    remember = BooleanField("Remember Me")

    submit = SubmitField("Login")
