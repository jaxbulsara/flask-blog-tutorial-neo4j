from flask_blog import User, Post, AUTHOR
from datetime import datetime
from py2neo.database import Graph

user_1 = User("Jenisha", "jenishatb@gmail.com", "password")

post_1 = Post("Post 3", datetime.utcnow(), "This is another post")

post_1_author = AUTHOR(user_1.__node__, post_1.__node__)

print(user_1)
print(post_1)
print(post_1_author)

graph = Graph(password="root")
graph.create(post_1_author)
